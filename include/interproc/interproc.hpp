#ifndef __LIBJSONINTERPROC_INTERPROC_HPP__
#define __LIBJSONINTERPROC_INTERPROC_HPP__

#include "../../jsoncpp-src-0.5.0/include/json/json.h"
#include <string>
#include <exception>
#include <functional>
#include <map>
#include <queue>
#include <mutex>
#include <thread>

namespace interproc {

class Exception : public std::exception {
	protected:
	std::string _reason,_type;
	public:
	Exception(std::string reason, std::string type) { _reason = reason; _type = type; }
	~Exception() throw() {}
	const char* what() { return _reason.c_str(); }
	const char* type() { return _type.c_str(); }
};

class NotConnectedException : public Exception {
	public:
	NotConnectedException(): Exception("Not connected to server.","notconnected") {}
};

class DuplicateEntryException : public Exception {
	public:
	DuplicateEntryException() : Exception("Duplicate entry.","duplicate") {}
};

class NoEntryException : public Exception {
	public:
	NoEntryException() : Exception("No such entry.","noentry") {}
};

class RemoteException : public Exception {
	public:
	RemoteException() : Exception("An error occurred on the other side. Probably unknown method.","remoteerror") {}
};

class NetlinkException : public Exception {
	public:
	NetlinkException(std::string a) : Exception(a,"netlink") {}
};

typedef std::function<Json::Value(Json::Value)> rpc_t;

class Connection {
	protected:
	//Yeah... This way I don't have to include the header for netlinksockets with this one, reduces compiletime for apps and I was only going to store a pointer anyway.
	void *sockptr;
	bool isConnected, mainLoop;
	std::map<std::string,rpc_t> rpcMap;
	std::queue<int> uidQueue;
	std::queue<Json::Value> packetQueue;
	std::queue<Json::Value> messageQueue;
	std::map<int,std::thread*> threadMap;
	std::map<int,bool> callWaiterMap;
	//HACK: I use a pointer here but it will never be deinitialized.
	std::map<int,Json::Value*> callRetMap;
	std::mutex sendMutex, pQtex, threadMutex, rpcMutex, cWtex, uidTex, msgTex, mainTex;
	void sendJson(Json::Value);
	int getUID();
	std::thread *rL, *pL;
	std::string myName;
	public:
	Connection();
	~Connection();
	//getter
	std::string getName() { return myName; }
	//Connect to dispatch server.
	void connect(std::string ip, int port, std::string clientName);
	//Disconnect
	void disconnect();
	//Call a remote method (throws remoteException on remote error)
	Json::Value call(std::string receiver, std::string methodName, Json::Value parameter);
	//Send remote
	void sendMessage(std::string receiver, Json::Value message);
	//Main loop
	void main();
	void killMain();
	void waitForMain();
	//Simple modifiers for the rpc calls we listen to.
	void addRPCMethod(std::string name, rpc_t method);
	void removeRPCMethod(std::string name);
	//messageQueue modification
	size_t messageSize() { msgTex.lock(); size_t rc = messageQueue.size(); msgTex.unlock(); return rc; }
	Json::Value getLastMessage() { msgTex.lock(); Json::Value rc = messageQueue.front(); messageQueue.pop(); msgTex.unlock(); return rc; }

	//IMPORTANT! NEVER CALL THESE! (These are used internally, but can't be made private/protected)
	void makeCall(Json::Value);
	void readerLoop();
	void processLoop();
};

}

#endif

