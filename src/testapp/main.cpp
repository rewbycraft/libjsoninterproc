#include <iostream>
#include <interproc/interproc.hpp>
#include <thread>

using namespace std;

interproc::Connection conn;

Json::Value hello(Json::Value v)
{
	cout << "Received: " << v.asString() << endl;
	return v;
}

int main(int argc, char *argv[])
{
	cout << "Connection init." << endl;
	if (argc>1)
	{
		conn.connect("127.0.0.1",1234,argv[1]);
	}
	else
		return 1;
	conn.addRPCMethod("hello",hello);
	cout << "Inducing main." << endl;
	conn.main();
	if (argc>3)
	{
		cout << "Calling..." << endl;
		Json::Value rc = conn.call(argv[2],"hello",argv[3]);
		cout << "Call made! Returned: " << rc.asString() << endl << flush;
		conn.killMain();
		conn.disconnect();
		return 0;
	}
	else
	{
		cout << "Waiting for join..." << endl;
		conn.waitForMain();
		return 0;
	}
}

