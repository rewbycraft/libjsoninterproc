#include <iostream>
#include <netlink/netlink.h>
#include <json/json.h>
#include <sstream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>

using namespace std;

namespace setting
{
	int port, uidAlloc;
}

namespace global
{
	mutex uidTex, threadTex, tInfoTex, sidTex;
	int uid;
	map<int, thread*> threadMap;
	map<int, bool> threadMayRunMap;
	map<int, string> nameMap;
	map<int, NL::Socket*> sockMap;
	map<NL::Socket*,int > sidMap;
}

namespace NL
{
	int getGroupId(Socket* sock, SocketGroup* group)
	{
		for (unsigned i = 0; i < (unsigned)group->size(); i++)
			if (group->get(i)==sock)
				return i;
		printf("WARNING: Socket not found.\n");
		return -1;
	}
}

NL::Socket *getSocket(string name)
{
	NL::Socket* rc = 0;
	global::tInfoTex.lock();
	for (auto k : global::nameMap)
		if (k.second == name)
		{
			rc = global::sockMap[k.first];
			break;
		}
	global::tInfoTex.unlock();
	return rc;
}

int getUID()
{
	global::uidTex.lock();
	int uid = global::uid++;
	global::uidTex.unlock();
	return uid;
}

int getSockId(NL::Socket *s)
{
	global::sidTex.lock();
	for (auto p : global::sidMap)
		if (p.first==s)
		{
			global::sidTex.unlock();
			return p.second;
		}
	global::sidMap[s] = getUID();
	int rc = global::sidMap[s];
	global::sidTex.unlock();
	return rc;
}

void freeSockId(NL::Socket *s)
{
	global::sidTex.lock();
	global::sidMap.erase(s);
	global::sidTex.unlock();
}

Json::Value readPacket(NL::Socket *sock)
{
	Json::Value cP;
	cP["_okay?"] = false;
	if (!(sock->nextReadSize()>0))
		return cP;
	stringstream a;
	char c = 0;
	while (c!='|')
	{
		if (c!=0)
			a<<c;
		sock->read(&c,sizeof(char));
	}
	int size;
	a>>size;
	char *buf = (char*) malloc(size);
	sock->read(buf,size);
	Json::Reader reader;
	Json::Value v;
	//Note: Corrupted packets are discarded.
	//      I'm assuming libjsoncpp produces valid data.
	//TODO: Add graceful handling of corrupted packets.
	if (reader.parse(buf,v))
	{
		free(buf);
		v["_okay?"]=true;
		return v;
	}
	free(buf);
	cout << "WARNING: Corrupted packet dropped." << endl;
	return cP;
}

void writePacket(NL::Socket *sock, Json::Value v)
{
	if (!sock)
		return;
	stringstream packet, tmp;
	tmp << v;
	packet << tmp.str().size() << "|" << tmp.str();
	sock->send(packet.str().c_str(),packet.str().size());
}

void handler(NL::Socket* sock, NL::SocketGroup *sockgroup)
{
	try {
	int tId = getSockId(sock);
	cout << "Connectionhandler for connection " << tId << " has launched." << endl << flush;
	bool running = true;
	while (running)
	{
		Json::Value packet = readPacket(sock);
		if (packet["_okay?"].asBool())
		{
//			cout << tId << " is processing a packet of type: " << packet["type"].asString() << "..." << endl << flush;
			if (packet["type"].asString() == "getuid")
			{
				cout << "Allocating " << setting::uidAlloc << " uids to " << tId << "..." << endl << flush;
				Json::Value ret;
				ret["type"] = "getuid";
				ret["payload"]["size"] = setting::uidAlloc;
				for (int i = 0; i < setting::uidAlloc; i++)
					ret["payload"]["uids"][i] = getUID();
				writePacket(sock,ret);
			} else if (packet["type"].asString() == "register") {
				if (packet["payload"]["type"].asString()!="rw")
				{
					//Anonymous client.
					stringstream name;
					name << "__" << getUID();
					global::tInfoTex.lock();
					global::nameMap[tId] = name.str();
					cout << "Connection " << tId << " registered as " << global::nameMap[tId] << endl;
					global::tInfoTex.unlock();
					Json::Value v;
					v["type"] = "setname";
					v["payload"] = name.str();
					writePacket(sock,v);
				}
				else
				{
					global::tInfoTex.lock();
					global::nameMap[tId] = packet["payload"]["name"].asString();
					cout << "Connection " << tId << " registered as " << global::nameMap[tId] << endl;
					global::tInfoTex.unlock();
				}
			} else {
				//Whatever, we don't really need to do anything with these.
				cout << "Connection " << tId << " decided to forward the packet to " << packet["target"].asString() << "..." << endl;
				writePacket(getSocket(packet["target"].asString()),packet);
			}
		}
		this_thread::sleep_for(chrono::milliseconds(LATENCY));
		global::tInfoTex.lock();
		running = global::threadMayRunMap[tId];
		global::tInfoTex.unlock();
	}

	} catch (NL::Exception e) {
		cerr << endl << "**HANDLER EXCEPTION**: " << e.msg() << "\t" << e.nativeErrorCode() << endl << endl << flush;
		//terminate();
	}
}

class OnAccept: public NL::SocketGroupCmd {
	void exec(NL::Socket* ssocket, NL::SocketGroup* group, void* reference) {
		NL::Socket* socket = ssocket->accept();
		group->add(socket);
		int tId = getSockId(socket);
		cout << "New connection from " << socket->hostTo() << ":" << socket->portTo() << " CONNID: " << tId << "!" << endl << flush;
		global::tInfoTex.lock();
			global::threadMayRunMap[tId] = true;
			global::sockMap[tId] = socket;
		global::tInfoTex.unlock();
		global::threadTex.lock();
			global::threadMap[tId] = new thread(handler,socket,group);
		global::threadTex.unlock();
	}
};

class OnDisconnect: public NL::SocketGroupCmd {
	void exec(NL::Socket* socket, NL::SocketGroup* group, void* reference) {
		int tId = getSockId(socket);
		cout << "Client " << socket->hostTo() << " (" << tId << ") disconnected..." << endl << flush;
		group->remove(socket);
		socket->disconnect();
		delete socket;

		global::tInfoTex.lock();
			global::threadMayRunMap[tId] = false;
		global::tInfoTex.unlock();
		global::threadTex.lock();
			global::threadMap[tId]->join();
			delete global::threadMap[tId];
			global::threadMap.erase(tId);
		global::threadTex.unlock();
		global::tInfoTex.lock();
			global::threadMayRunMap.erase(tId);
			global::sockMap.erase(tId);
			global::nameMap.erase(tId);
		global::tInfoTex.unlock();
		freeSockId(socket);
	}
};

int main(int argc, char *argv[])
{
	NL::init();
	try {
		cerr << "libjsoninterproc dispatch server" << endl;
		cerr << " Made by Roelf Software" << endl;
		cerr << " Licensed under RSPL. Copy included with source." << endl;
		cerr << endl << flush;
		setting::port = -1;
		setting::uidAlloc = 10;
		global::uid = 0;
		bool needHelp = false;
		for (int i = 1; i < argc; i++)
		{
			if (strcmp(argv[i],"-p")==0)
			{
				stringstream a;
				a << argv[++i];
				a >> setting::port;
			} else if (strcmp(argv[i],"-u")==0)
			{
				stringstream a;
				a << argv[++i];
				a >> setting::uidAlloc;
			} else if (strcmp(argv[i],"-h")==0)
				needHelp=true;
			else
				cout << "WARNING: Parameter " << argv[i] << " has been ignored." << endl; 
		}
		needHelp = needHelp || (setting::port==-1);
		if (needHelp)
		{
			cerr << "Usage: " << argv[0] << " -p <port number> [-u <uids to allocate to one client>]" << endl;
			return !(setting::port==-1);
		}
		cout << "Settings:" << endl;
		cout << "* port:\t\t" << setting::port << endl;
		cout << "* uidAlloc:\t" << setting::uidAlloc << endl;
		cout << endl;
		cout << "Launching dispatcher on port " << setting::port << "..." << endl << flush;
		NL::Socket socketServer(setting::port);
		NL::SocketGroup group;
		OnAccept onAccept;
		OnDisconnect onDisconnect;
		group.setCmdOnAccept(&onAccept);
		group.setCmdOnDisconnect(&onDisconnect);
		group.add(&socketServer);
		while(true)
			group.listen(LATENCY);
	} catch (NL::Exception e) {
		cerr << endl << "**EXCEPTION**: " << e.msg() << endl << endl << flush;
		terminate();
	}
}

