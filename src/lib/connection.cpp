#include <interproc/interproc.hpp>
#include <netlink/netlink.h>
#include <sstream>
#include <chrono>

using namespace std;

#define THROWEX(x) throw *(new x);
#define SOCK ((NL::Socket*)sockptr)

namespace interproc {

Connection::Connection()
{
	isConnected = false;
	mainLoop = false;
	NL::init();
}

#define EM(x) \
	{ \
		for (auto d : x) \
		{ \
			x.erase(d.first); \
		} \
	}

#define EQ(x) \
	{ \
		while (x.size()) \
			x.pop(); \
	}

Connection::~Connection()
{
	if (isConnected)
		disconnect();
	EM(rpcMap)
	EM(threadMap)
	EM(callWaiterMap)
	EM(callRetMap)
	EQ(packetQueue)
	EQ(messageQueue)
}

#undef EM
#undef EQ

void Connection::sendJson(Json::Value v)
{
	stringstream packet, tmp;
	tmp << v;
	packet << tmp.str().size() << "|" << tmp.str();
	sendMutex.lock();
	SOCK->send(packet.str().c_str(),packet.str().size());
	sendMutex.unlock();
}

//Connect to dispatch server.
void Connection::connect(std::string ip, int port, std::string clientName)
{
	try {
		sockptr = (void*)new NL::Socket(ip,port);
	} catch (NL::Exception e) {
		THROWEX(NetlinkException(e.msg()));
	}
	isConnected = true;
	myName = clientName;

	Json::Value thingy;
	thingy["type"] = "register";
	if (myName.size())
		thingy["payload"]["type"] = "rw";
	else
		thingy["payload"]["type"] = "wo";
	thingy["payload"]["name"] = clientName;
	sendJson(thingy);

	//Not required anymore.
	/*Json::Value v;
	v["type"] = "getuid";
	v["sender"] = myName;
	sendJson(v);*/
}

//Disconnect
void Connection::disconnect()
{
	killMain();
	SOCK->disconnect();
	isConnected = false;
}

int Connection::getUID()
{
	uidTex.lock();
	int qs = uidQueue.size();
	uidTex.unlock();
	if (qs <= 1)
	{
		//We're about to use our last one! Get some more before we run out!
		Json::Value v;
		v["type"] = "getuid";
		v["sender"] = myName;
		sendJson(v);
	}
	//Wait for more uids from server!
	while (qs == 0)
	{
		this_thread::sleep_for(chrono::milliseconds(LATENCY));
		uidTex.lock();
		qs = uidQueue.size();
		uidTex.unlock();
	}
	uidTex.lock();
	int rc = uidQueue.front();
	uidQueue.pop();
	uidTex.unlock();
	return rc;
}

//Call a remote method
Json::Value Connection::call(std::string receiver, std::string methodName, Json::Value parameter)
{
	int uid = getUID();
	Json::Value v;
	v["target"]   = receiver;
	v["sender"]   = myName;
	v["type"]     = "call";
	v["calltype"] = "rpc";
	v["uniqueid"] = uid;
	v["callname"] = methodName;
	v["payload"] = parameter;
	cWtex.lock();
	callWaiterMap[uid] = true;
	bool w = true;
	cWtex.unlock();
	sendJson(v);
	while (w)
	{
		this_thread::sleep_for(chrono::milliseconds(LATENCY));
		cWtex.lock();
		w = callWaiterMap[uid];
		cWtex.unlock();
	}
	cWtex.lock();
	Json::Value ret = *(callRetMap[uid]);
	delete callRetMap[uid];
	callRetMap.erase(uid);
	cWtex.unlock();
	if (ret["error"].asBool())
		THROWEX(RemoteException);
	return ret["payload"];
}

//Send remote
void Connection::sendMessage(std::string receiver, Json::Value message)
{
	Json::Value v;
	v["type"] = "call";
	v["calltype"] = "msg";
	v["sender"] = myName;
	v["target"] = receiver;
	v["payload"] = message;
	sendJson(v);
}

void _readerLoop(Connection *c)
{
	c->readerLoop();
}

void _processLoop(Connection *c)
{
	c->processLoop();
}

//Main loop
void Connection::main()
{
	if (!isConnected)
		THROWEX(NotConnectedException)
	if (mainLoop)
		return;
	mainLoop=true;
	rL = new thread(_readerLoop,this);
	pL = new thread(_processLoop,this);
}

void Connection::waitForMain()
{
	if (!mainLoop)
		return;
	rL->join();
	pL->join();
}

void Connection::killMain()
{
	if (!mainLoop)
		return;
	mainLoop = false;
	/*if (pL->joinable())
		pL->join();
	if (rL->joinable())
		rL->join();*/
}

void _makeCall(Connection *c, Json::Value v)
{
	c->makeCall(v);
}

void Connection::makeCall(Json::Value v)
{
	Json::Value ret;
	ret["type"] = "ret";
	ret["uniqueid"] = v["uniqueid"];
	ret["sender"] = myName;
	ret["target"] = v["sender"];
	ret["callname"] = v["callname"];

	rpcMutex.lock();
	if (!rpcMap[v["callname"].asString()])
	{
		rpcMutex.unlock();
		ret["error"] = true;
		sendJson(ret);
		return;
	}
	rpc_t cl = rpcMap[v["callname"].asString()];
	rpcMutex.unlock();
	ret["error"] = false;
	ret["payload"] = cl(v["payload"]);
	sendJson(ret);
}

void Connection::processLoop()
{
	if (!isConnected)
		THROWEX(NotConnectedException)
	mainTex.lock();
	while (mainLoop)
	{
		mainTex.unlock();
		//I know it's unsafe.
		pQtex.lock();
		if (!packetQueue.size())
		{
			pQtex.unlock();
			this_thread::sleep_for(chrono::milliseconds(LATENCY));
			continue;
		}
		pQtex.unlock();
		pQtex.lock();
		Json::Value v = packetQueue.front();
		packetQueue.pop();
		pQtex.unlock();
		if ((v["type"].asString() == "call") && (v["calltype"].asString() == "rpc"))
		{
			threadMutex.lock();
			//TODO: Add way to remove thread from map. ('Cause this is basically a memory leak)
			threadMap[v["uniqueid"].asInt()] = new thread(_makeCall,this,v);
			threadMutex.unlock();
		}
		if ((v["type"].asString() == "call") && (v["calltype"].asString() == "msg"))
		{
			msgTex.lock();
			messageQueue.push(v["payload"]);
			msgTex.unlock();
		}
		if (v["type"].asString() == "setname")
		{
			myName = v["payload"].asString();
		}
		if (v["type"].asString() == "ret")
		{
			//TODO: Add way to remove these entries from the maps. Basically memoryleak.
			cWtex.lock();
			callRetMap[v["uniqueid"].asInt()] = new Json::Value(v);
			callWaiterMap[v["uniqueid"].asInt()] = false;
			cWtex.unlock();
		}
		if (v["type"].asString() == "getuid")
		{
			int sz = v["payload"]["size"].asInt();
			for (int i = 0 ; i < sz; i++)
			{
				int id = v["payload"]["uids"][i].asInt();
				uidTex.lock();
				uidQueue.push(id);
				uidTex.unlock();
			}
		}
	}
	mainTex.unlock();
}

void Connection::readerLoop()
{
	if (!isConnected)
		THROWEX(NotConnectedException)
	mainTex.lock();
	while (mainLoop)
	{
		mainTex.unlock();
		if (!SOCK->nextReadSize())
		{
			this_thread::sleep_for(chrono::milliseconds(LATENCY));
			continue;
		}
		stringstream a;
		char c = 0;
		while (c!='|')
		{
			if (c!=0)
				a<<c;
			SOCK->read(&c,sizeof(char));
		}
		int size;
		a>>size;
		char *buf = (char*) malloc(size);
		SOCK->read(buf,size);
		Json::Reader reader;
		Json::Value v;
		//Note: Corrupted packets are discarded. Calls which require a return packet will stall.
		//      I'm assuming libjsoncpp produces valid data.
		//TODO: Add graceful handling of corrupted packets.
		string tmp;
		tmp.append(buf,size);
		if (reader.parse(tmp,v))
		{
			pQtex.lock();
			packetQueue.push(v);
			pQtex.unlock();
		}
		free(buf);
	}
	mainTex.unlock();
}

//Simple modifiers for the rpc calls we listen to.
void Connection::addRPCMethod(std::string name, rpc_t method)
{
	rpcMutex.lock();
	if (rpcMap[name])
	{
		rpcMutex.unlock();
		THROWEX(DuplicateEntryException)
	}
	rpcMap[name] = method;
	rpcMutex.unlock();
}
void Connection::removeRPCMethod(std::string name)
{
	rpcMutex.lock();
	for (auto p : rpcMap)
		if (p.first == name)
		{
			rpcMap.erase(name);
			rpcMutex.unlock();
			return;
		}
	rpcMutex.unlock();
	THROWEX(NoEntryException)
}

}

