JSONCPP_FILES     = $(shell find ./jsoncpp-src-0.5.0/src/lib_json -type f -iname "*.cpp")
JSONCPP_OBJECTS   = $(JSONCPP_FILES:.cpp=.cpp.o)
JSONCPP_FLAGS     = -I./jsoncpp-src-0.5.0/include
NETLINK_FILES     = $(shell find ./netLink/src -type f -iname "*.cc")
NETLINK_OBJECTS   = $(NETLINK_FILES:.cc=.cc.o)
NETLINK_FLAGS     = -I./netLink/include

LIB_FILES         = $(shell find ./src/lib -type f -iname "*.cpp")
LIB_HEADERS       = $(shell find ./include -type f -iname "*.hpp")
LIB_BIN           = $(shell find ./src/bin -type f -iname "*.cpp")
LIB_OBJECTS       = $(LIB_FILES:.cpp=.cpp.o)
LIB_BINOBJECTS    = $(LIB_BIN:.cpp=.cpp.o)
LIB_FLAGS         = -I./include -I./jsoncpp-src-0.5.0/include -I./netLink/include -std=gnu++0x -pthread -g -ggdb -DLATENCY=25
LIB_BINFLAGS      = $(LIB_FLAGS) -Llib -lnetlink -ljsoncpp -g -ggdb

TEST_FILES        = $(shell find ./src/testapp -type f -iname "*.cpp")
TEST_OBJECTS      = $(TEST_FILES:.cpp=.cpp.o)
TEST_FLAGS        = $(LIB_FLAGS) -Llib -ljsoninterproc_full -g -ggdb

all: dependencies libjsoninterproc libjsoninterproc_full dispatchbin testapp
.PHONY: all

dependencies: jsoncpp netlink
.PHONY: dependencies

clean:
	@echo "[@]\tCleaning all files..."
	@echo "[@]\tRemoving object files (if any)..."
	@for i in $(shell find . -type f -iname "*.o") ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tRemoving compiled libraries (if any)..."
	@for i in $(shell find . -type f -iname "*.a") ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tRemoving compiles binaries (if any)..."
	@for i in $(shell find ./bin -type f) ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tCleaning complete!"

jsoncpp:
	@echo "[@]\tCompiling JSONCPP..."
	@echo "[@]\tFlags: $(JSONCPP_FLAGS)"
	@echo "[@]\tFiles: $(JSONCPP_FILES)"
	@for i in $(JSONCPP_FILES) ; do echo "[g++]\tCompiling $$i..." ; g++ -c $$i -o $$i.o $(JSONCPP_FLAGS) ; done
	@echo "[ar]\tArchiving $(JSONCPP_OBJECTS) into libjsoncpp.a..."
	-@rm ./lib/libjsoncpp.a 2>/dev/null
	@ar crf ./lib/libjsoncpp.a $(JSONCPP_OBJECTS)
	@for i in $(JSONCPP_OBJECTS) ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tCompilation successfull."
.PHONY: jsoncpp

netlink:
	@echo "[@]\tCompiling NETLINK..."
	@echo "[@]\tFlags: $(NETLINK_FLAGS)"
	@echo "[@]\tFiles: $(NETLINK_FILES)"
	@for i in $(NETLINK_FILES) ; do echo "[g++]\tCompiling $$i..." ; g++ -c $$i -o $$i.o $(NETLINK_FLAGS) ; done
	@echo "[ar]\tArchiving $(NETLINK_OBJECTS) into libnetlink.a..."
	-@rm ./lib/libnetlink.a 2>/dev/null
	@ar crf ./lib/libnetlink.a $(NETLINK_OBJECTS)
	@for i in $(NETLINK_OBJECTS) ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tCompilation successfull."

.PHONY: netlink

libjsoninterproc:
	@echo "[@]\tCompiling JSONINTERPROC (library mode)..."
	@echo "[@]\tFlags: $(LIB_FLAGS)"
	@echo "[@]\tFiles: $(LIB_FILES)"
	@for i in $(LIB_FILES) ; do echo "[g++]\tCompiling $$i..." ; g++ -c $$i -o $$i.o $(LIB_FLAGS) ; done
	@echo "[ar]\tArchiving $(LIB_OBJECTS) into libjsoninterproc.a..."
	-@rm ./lib/libjsoninterproc.a 2>/dev/null
	@ar crf ./lib/libjsoninterproc.a $(LIB_OBJECTS)
	@for i in $(LIB_OBJECTS) ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tCompilation successfull."

dispatchbin:
	@echo "[@]\tCompiling JSONINTERPROC (binary mode)..."
	@echo "[@]\tFlags: $(LIB_FLAGS)"
	@echo "[@]\tFiles: $(LIB_BIN)"
	@for i in $(LIB_BIN) ; do echo "[g++]\tCompiling $$i..." ; g++ -c $$i -o $$i.o $(LIB_FLAGS) ; done
	@echo "[g++]\tLinking $(LIB_BINOBJECTS) into ./bin/libjsoninterproc_dispatcher..."
	-@rm ./bin/libjsoninterproc_dispatcher 2>/dev/null
	@g++ $(LIB_BINOBJECTS) $(LIB_BINFLAGS) -o bin/libjsoninterproc_dispatcher
	@for i in $(LIB_BINOBJECTS) ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tCompilation successfull."

testapp: libjsoninterproc_full dispatchbin
	@echo "[@]\tCompiling TESTAPP..."
	@echo "[@]\tFlags: $(TEST_FLAGS)"
	@echo "[@]\tFiles: $(TEST_FILES)"
	@for i in $(TEST_FILES) ; do echo "[g++]\tCompiling $$i..." ; g++ -c $$i -o $$i.o $(TEST_FLAGS) ; done
	@echo "[g++]\tLinking $(TEST_OBJECTS) into ./bin/testapp..."
	-@rm ./bin/testapp 2>/dev/null
	@g++ $(TEST_OBJECTS) $(TEST_FLAGS) -o bin/testapp
	@for i in $(TEST_OBJECTS) ; do echo "[rm]\tRemoving $$i..." ; rm $$i ; done
	@echo "[@]\tCompilation successfull."


libjsoninterproc_full: libjsoninterproc dependencies
	@echo "[@]\tMerging libraries into one file..."
	@echo "[mkdir]\tCreating temporary directory..."
	@mkdir -p mergetmp
	@for i in libjsoncpp.a libnetlink.a libjsoninterproc.a ; do echo "[ar]\tExtracting $$i..." && cd mergetmp && ar x ../lib/$$i && cd .. ; done
	@echo "[ar]\tRepackaging files..."
	@ar crf ./lib/libjsoninterproc_full.a ./mergetmp/*
	@echo "[rm]\tCleaning..."
	@rm -rf mergetmp
	@echo "[@]\tFinished creating libjsoninterproc_full.a..."
.PHONY: libjsoninterproc_full

edit:
	@gedit Makefile $(LIB_FILES) $(LIB_BIN) $(TEST_FILES) $(LIB_HEADERS) &

